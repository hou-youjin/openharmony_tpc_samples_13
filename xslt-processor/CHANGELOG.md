## v1.0.0
    
xslt-processor支持以下能力：
* 支持解析XML字符串为可操作对象。
* 进行XSLT转换。
* 构建xpath表达式计算上下文及计算。
* 支持xpath表达式解析。