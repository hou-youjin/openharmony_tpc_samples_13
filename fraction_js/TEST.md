## fraction.js单元测试用例

该测试用例基于OpenHarmony系统下进行单元测试

### 单元测试用例覆盖情况

| 接口名                                                 | 是否通过  | 备注        |
|-----------------------------------------------------|-------|-----------|
| add(num: number &#124; string)                      | pass  |
| div(num: number &#124; string)                      | pass  |
| sub(num: number &#124; string)                      | pass  |
| mul(num: number &#124; string)                      | pass  |
| pow(num: number &#124; string)                      | pass  |
| abs()                                               | pass  |
| ceil(places?: number)                               | pass  |
| floor(places?: number)                              | pass  |
| round(places?: number)                              | pass  |
| neg()                                               | pass  |
| mod(n?: number &#124; string &#124; Fraction)       | pass  |
| inverse()                                           | pass  |
| equals(n: number &#124; string &#124; Fraction)     | pass  |
| clone()                                             | pass  |
| toString(decimalPlaces?: number)                    | pass  |
| compare(n: number &#124; string &#124; Fraction)    | pass  |
| gcd(num: number &#124; string)                      | pass  |
| lcm(num: number &#124; string)                      | pass  |
| simplify(eps?: number)                              | pass  |
| divisible(n: number &#124; string &#124; Fraction)  | pass  |
| valueOf()                                           | pass  |
| toLatex(excludeWhole?: boolean)                     | pass  |
| toFraction(excludeWhole?: boolean)                  | pass  |
| toContinued()                                       | pass  |