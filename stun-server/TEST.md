## stun单元测试用例

该测试用例基于OpenHarmony系统环境进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|stunServer.setServerMessageListener|pass|
|stunServer.createServer|pass|
|stunClient.createClient|pass|
|stunClient.setClientMessageListener|pass|