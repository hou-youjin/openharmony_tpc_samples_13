# Changelog
## [v1.0.0] 2023-12

基于[jszip](https://github.com/xqdoo00o/jszip) 原库3.5.0版本进行适配，使其可以运行在 OpenHarmony，并沿用其现有用法和特性。