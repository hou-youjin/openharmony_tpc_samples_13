﻿## dom_serializer 单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/cheeriojs/dom-serializer/blob/master/src/index.spec.ts) 进行单元测试

**单元测试用例覆盖情况**

### dom_serializer

| 接口名                                                        | 是否通过 | 备注 |
|------------------------------------------------------------| ------- | ---- |
| render(node: AnyNode | ArrayLike<AnyNode>, options?: DomSerializerOptions): string | pass    |      |
